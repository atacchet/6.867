load('../big/dsetlesbegues.mat');

ytr = ytr(:,1);
yte = yte(:,1);

name = 'smplesbegues';
opt = defopt(name);
opt.seq = {'paramsel:loocvprimal','paramsel:calibratesmp','rls:smp','pred:primal','perf:rmse'};
opt.process{1} = [3,2,2,0,0];
opt.process{2} = [3,3,3,2,2];

opt.epochs = 10;
opt.smp_subsize = 6000;
opt.smp_nestimates = 10;
opt.nlambda = 100;
opt.hoperf = @perf_rmse;


gurls(Xtr, ytr, opt, 1);
gurls(Xte, yte, opt, 2);
