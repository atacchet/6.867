load('big/mnist_train');
load('big/mnist_test');

xtr = double(Xtr);
xte = double(Xte);
codes = 2*eye(10) - 1;
yytr = codes(ytr,:);
yyte = codes(yte,:);


wpath = '/home/atacchet/Documents/MIT/6.867/repo/6.867/proj/testscripts'

Xtr = bigarray_mat(fullfile(wpath,'big/mnistXtr'));
Xtr.Clear();
Xtr.Init(2000);
Xtr.Append(xtr');

%% Full traning labels.
ytr = bigarray_mat(fullfile(wpath,'big/mnistytr'));
ytr.Clear();
ytr.Init(2000);
ytr.Append(yytr');

%% Test set.
Xte = bigarray_mat(fullfile(wpath,'/big/mnistXte'));
Xte.Clear();
Xte.Init(2000);
Xte.Append(xte');

%% Test set labels.
yte = bigarray_mat(fullfile(wpath,'/big/mnistyte'));
yte.Clear();
yte.Init(2000);
yte.Append(yyte');

Xtr.Flush();
ytr.Flush();
Xte.Flush();
yte.Flush();

