function gentoydatabig(fun, xtr, xte, opt)
wpath = '/home/atacchet/Documents/MIT/6.867/repo/6.867/proj/testscripts'

Xtr = bigarray_mat(fullfile(wpath,'big/trainX'));
Xtr.Clear();
Xtr.Init(2000);

%% Full traning labels.
ytr = bigarray_mat(fullfile(wpath,'big/trainY'));
ytr.Clear();
ytr.Init(2000);

%% Test set.
Xte = bigarray_mat(fullfile(wpath,'/big/testX'));
Xte.Clear();
Xte.Init(2000);

%% Test set labels.
yte = bigarray_mat(fullfile(wpath,'/big/testY'));
yte.Clear();
yte.Init(2000);

Xva = bigarray_mat(fullfile(wpath,'big/valX'));
Xva.Clear();
Xva.Init(2000);


yva = bigarray_mat(fullfile(wpath,'big/valY'));
yva.Clear();
yva.Init(2000);


%% Actually generate the dataset copying data in the bigarrays.

for i = 1:size(xtr,1)
		y = fun(xtr(i,:));
		x = opt.featuremap(xtr(i,:));

		if opt.noise
			y = y + opt.noisesigma*randn(1);
		end

		coin = rand(1);
		if coin > opt.hoproportion
			Xva.Append(x');
			yva.Append(y);
		end

		Xtr.Append(x');
		ytr.Append(y);
		fprintf('Processing point %d: %.2f%%\r',i, i*100/size(xtr,1));
end
fprintf('\n');
for i = 1:size(xte,1)
		y = fun(xte(i,:));
		x = opt.featuremap(xte(i,:));
		Xte.Append(x');
		yte.Append(y);
		fprintf('Processing point %d: %.2f%%\r',i, i*100/size(xte,1));
end
	

fprintf('\n');
		
%% Make sure everything is written to disk.

Xtr.Flush();
ytr.Flush();
Xte.Flush();
yte.Flush();
Xva.Flush();
yva.Flush();
