load('big/dsetlesbegues.mat');

name = 'sgdregressionlesbegues';
opt = defopt(name);
opt.hoperf = @perf_rmse;

opt.seq = {'paramsel:sgd', 'rls:sgd','pred:primal','perf:rmse'};
opt.process{1} = [2,2,0,0];
opt.process{2} = [3,3,2,2];
opt.sgd.subsize = 3e3;

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);


%Xtr = bigarray_mat('big/trainX');
%ytr = bigarray_mat('big/trainY');
%
%Xte = bigarray_mat('big/testX');
%yte = bigarray_mat('big/testY');
%
%Xtr.Transpose(true);
%Xte.Transpose(true);
%ytr.Transpose(true);
%yte.Transpose(true);
%
%name = 'linsgdbatch';
%
%opt = bigdefopt(name);
%opt.seq = {'paramsel:hoprimal','rls:primal','pred:primal','perf:rmse'};
%opt.process{1} = [2,2,0,0];
%opt.process{2} = [3,3,2,2];
%opt.sgd.subsize = 2e3;
%opt.hoperf = @bigperf_rmse;
%opt.nlambda = 100;
%opt.files.Xva_filename = fullfile(pwd, 'big/valX');
%opt.files.yva_filename = fullfile(pwd, 'big/valY');
%opt.files.pred_filename = fullfile(pwd, 'big' , 'pred');
%
%biggurls(Xtr, ytr, opt, 1);
%biggurls(Xtr, ytr, opt, 2);
