load(fullfile(gurls_root,'demo/data/yeast_data.mat'));

name = 'lingd';
opt = defopt(name);
opt.seq = {'split:ho','paramsel:hoprimalgd','rls:primalgd','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,0,0];
opt.process{2} = [3,3,3,2,2];
gurls (Xtr, ytr, opt,0);
gurls (Xte, yte, opt,2);

name = 'lindualgd'

opt = defopt(name);
opt.seq = {'split:ho','kernel:linear','paramsel:hodualgd','rls:dualgd','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];
gurls (Xtr, ytr, opt,1);
gurls (Xte, yte, opt,2);

