function xf = fourier(x)
		nf = 20;
		T = 10;
		w = 2*pi/T;
		xf(1) = 1;
		f = 1;
		for k = 2:2:nf
			xf(k) 		= cos((f)*w*x);
			xf(k+1) 	= sin((f)*w*x);
			f = f+1;
		end
