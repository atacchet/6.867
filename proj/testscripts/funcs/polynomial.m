function xf = polynomial(x)
		nf = 5;
		for k = 1:nf
			xf(k) = x^k;
		end
