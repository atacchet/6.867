load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/dsetlesbegues');

t = load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/report/batch/lesbegues/full/lesbeguesfulllinloo_1.mat');
p = -3:3

for i = 1:numel(p)
		name = sprintf('lesbeguesbatchlambdasweep_%d',i);
		opt = defopt(name);
		opt.paramsel.lambdas = (10 ^ p(i)) * t.opt.paramsel.lambdas;
		opt.hoperf = @perf_rmse;
		
		opt.seq = {'rls:primal','pred:primal','perf:rmse'};
		opt.process{1} = [2,0,0];
		opt.process{2} = [3,2,2];
		
		gurls(Xtr, ytr, opt,1);
		gurls(Xte, yte, opt,2);
		
end
