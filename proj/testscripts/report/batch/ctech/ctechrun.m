%for i = 1:1
%	datadir = sprintf('/cbcl/scratch02/atacchet/6.869/work/ctechsplit%d/big/',i);
%	Xtr = bigarray_mat(fullfile(datadir, 'trainX'));
%	ytr = bigarray_mat(fullfile(datadir, 'trainY'));
%	Xte = bigarray_mat(fullfile(datadir, 'testX'));
%	yte = bigarray_mat(fullfile(datadir, 'testY'));
%
%	Xtr.Transpose(true);
%	ytr.Transpose(true);
%	Xte.Transpose(true);
%	yte.Transpose(true);
%
%	Xtr = Xtr(1:Xtr.NumItems,:);
%	Xte = Xte(1:Xte.NumItems,:);
%
%	ytr = ytr(1:ytr.NumItems,:)';
%	yte = yte(1:yte.NumItems,:)';
%
%	fname = fullfile('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/ctech',sprintf('ctech%d', i));
%	save(fname, 'Xtr','Xte','ytr','yte');
%
%end
%
%keyboard

%for i = 1:1
%	fname = fullfile('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/ctech',sprintf('ctech%d', i));
%	load(fname);
%
%	name = sprintf('ctechlinloo_%d',i);
%	opt = defopt(name);
%	opt.seq = {'kernel:linear','paramsel:loocvdual','rls:dual','pred:dual','perf:macroavg'};
%	opt.process{1} = [2,2,2,0,0];
%	opt.process{2} = [3,3,3,2,2];
%	gurls(Xtr,ytr,opt,1);
%	gurls(Xte,yte,opt,2);
%
%end

load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/ctech');
name = sprintf('ctechlinloo_%d',1);
opt = defopt(name);
opt.seq = {'kernel:linear','paramsel:loocvdual','rls:dual','pred:dual','perf:macroavg'};
opt.process{1} = [2,2,2,0,0];
opt.process{2} = [3,3,3,2,2];
gurls(Xtr,ytr,opt,1);
gurls(Xte,yte,opt,2);
