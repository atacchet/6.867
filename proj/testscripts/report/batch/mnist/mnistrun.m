%for i = 1:5
%	fname = fullfile('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist',sprintf('mtt%d',i));
%	load(fname);
%
%	Xtr = double(Xtr);
%	Xte = double(Xte);
%
%	name = sprintf('mnistlinloo_%d',i);
%	opt = defopt(name);
%	opt.seq = {'paramsel:loocvprimal','rls:primal','pred:primal','perf:macroavg'};
%	opt.process{1} = [2,2,0,0];
%	opt.process{2} = [3,3,2,2];
%	gurls(Xtr,ytr,opt,1);
%	gurls(Xte,yte,opt,2);
%end

load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_train');
load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_test');

Xtr = double(Xtr);
Xte = double(Xte);

c = 2*eye(10) - 1;
ytr = c(ytr,:);
yte = c(yte,:);

name = sprintf('mnistlinloo_%d',1);
opt = defopt(name);
opt.seq = {'paramsel:loocvprimal','rls:primal','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,0,0];
opt.process{2} = [3,3,2,2];
gurls(Xtr,ytr,opt,1);
gurls(Xte,yte,opt,2);
