load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/ctech');
name = sprintf('ctechlingd_%d',1);
opt = defopt(name);
opt.seq = {'kernel:linear','split:ho','paramsel:hodualgd','rls:dualgd','pred:dual','perf:macroavg'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];
gurls(Xtr,ytr,opt,1);
gurls(Xte,yte,opt,2);
