load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/dsetlesbegues');
name = 'lesbeguesgd_2';
opt = defopt(name);
opt.hoperf = @perf_rmse;
opt.gd.maxiter = 500;

opt.seq = {'split:ho','paramsel:hoprimalgd', 'rls:primalgd','pred:primal','perf:rmse'};
opt.process{1} = [2,2,2,0,0];
opt.process{2} = [3,3,3,2,2];

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);


