%load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_train');
%load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_test');

load(fullfile(gurls_root, '..','testscripts','data','mnist_train'));
load(fullfile(gurls_root, '..','testscripts','data','mnist_test'));

Xtr = double(Xtr);
Xte = double(Xte);

c = 2*eye(10) - 1;
ytr = c(ytr,:);
yte = c(yte,:);

name = sprintf('mnistlingd300iter_%d',1);
opt = defopt(name);

Xtr = norm_zscore(Xtr, [], opt);
Xte = norm_testzscore(Xte, [], opt);
opt.gd.maxiter = 1000;
opt.gd.nu = 0;
Xtr = norm_zscore(Xtr, [], opt);
Xte = norm_testzscore(Xtr, [], opt);
opt.seq = {'split:ho','paramsel:hoprimalgd','rls:primalgd','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,0,0];
opt.process{2} = [3,3,3,2,2];
gurls(Xtr,ytr,opt,1);
gurls(Xte,yte,opt,2);
