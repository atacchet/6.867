load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_train');
load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_test');
t = load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/report/batch/mnist/3v8/mnistlinloo3v8_1.mat');
Xtr = double(Xtr);
Xte = double(Xte);

Xtr = Xtr(ytr == 4 | ytr == 9,:);
Xte = Xte(yte == 4 | yte == 9,:);
ytr = ytr(ytr == 4 | ytr == 9);
yte = yte(yte == 4 | yte == 9);

ytr(ytr == 4) = -1;
ytr(ytr == 9) = 1;

yte(yte == 4) = -1;
yte(yte == 9) = 1;

p = -3:3

for i = 1:numel(p)

	opt = defopt(sprintf('%s_%d', 'mnistlinloo3v8lambdasweep',i));
	opt.paramsel.lambdas = (10 ^ p(i)) * t.opt.paramsel.lambdas;
	opt.seq = {'rls:pegasos','pred:primal','perf:macroavg'};
	opt.process{1} = [2,0,0];
	opt.process{2} = [3,2,2];
	
	opt.epochs = 100;
	opt.smp_subsize = 3000;
	opt.smp_nestimates = 10;
	opt.nlambda = 100;
	
	Xtr = norm_zscore(Xtr, [], opt);
	Xte = norm_testzscore(Xte, [], opt);
	
	gurls(Xtr, ytr, opt, 1);
	gurls(Xte, yte, opt, 2);
end
