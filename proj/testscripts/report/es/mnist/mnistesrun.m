load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_train');
load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_test');

Xtr = double(Xtr);
Xte = double(Xte);

Xtr = Xtr(ytr == 4 | ytr == 9,:);
Xte = Xte(yte == 4 | yte == 9,:);
ytr = ytr(ytr == 4 | ytr == 9);
yte = yte(yte == 4 | yte == 9);

ytr(ytr == 4) = -1;
ytr(ytr == 9) = 1;

yte(yte == 4) = -1;
yte(yte == 9) = 1;


name = 'mnistes_1';
opt = defopt(name);
opt.hoperf = @perf_macroavg;

opt.seq = {'paramsel:calibratesmp','split:ho','paramsel:sgdes','rls:sgdes','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];

opt.hoproportion = 5000/size(Xtr,1);
split = split_ho(Xtr, ytr, opt);
Xtr = Xtr(split{1}.va,:);
ytr = ytr(split{1}.va,:);

opt.smp_subsize = min(3e3,size(Xtr,1));
opt.sgd.subsize = min(6e3,size(Xtr,1));
opt.smp_nestimates = 20;
opt.sgdes.numepochs = 5;
opt.hoproportion = 0.95;

Xtr = norm_zscore(Xtr, [], opt);
Xte = norm_testzscore(Xte, [], opt);

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);
