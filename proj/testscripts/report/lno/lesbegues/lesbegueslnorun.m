load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/dsetlesbegues');
name = 'lesbegueslno_1';
opt = defopt(name);
opt.hoperf = @perf_rmse;

opt.seq = {'paramsel:sgd', 'rls:sgd','pred:primal','perf:rmse'};
opt.process{1} = [2,2,0,0];
opt.process{2} = [3,3,2,2];
opt.sgd.subsize = 3e3;

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);


