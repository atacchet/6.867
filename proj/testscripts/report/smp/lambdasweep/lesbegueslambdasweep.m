load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/dsetlesbegues');
t = load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/report/smp/lesbegues/lesbeguessmp_1');

ytr = ytr(:,1);
yte = yte(:,1);

p = -3:3
for i = 1:numel(p)
	name = sprintf('%s_%d', 'sweepsmplesbegues',i);

	opt = defopt(name);
	opt.paramsel.lambdas = (10 ^ p(i)) * t.opt.paramsel.lambdas;
	opt.seq = {'paramsel:calibratesmp','rls:smp','pred:primal','perf:rmse'};
	opt.process{1} = [2,2,0,0];
	opt.process{2} = [3,3,2,2];
	
	opt.epochs = 10;
	opt.smp_subsize = 6000;
	opt.smp_nestimates = 10;
	opt.nlambda = 100;
	opt.hoperf = @perf_rmse;
	
	
	gurls(Xtr, ytr, opt, 1);
	gurls(Xte, yte, opt, 2);
end
