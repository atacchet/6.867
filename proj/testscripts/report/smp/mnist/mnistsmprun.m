load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_train');
load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_test');
Xtr = double(Xtr);
Xte = double(Xte);

Xtr = Xtr(ytr == 4 | ytr == 9,:);
Xte = Xte(yte == 4 | yte == 9,:);
ytr = ytr(ytr == 4 | ytr == 9);
yte = yte(yte == 4 | yte == 9);

ytr(ytr == 4) = -1;
ytr(ytr == 9) = 1;

yte(yte == 4) = -1;
yte(yte == 9) = 1;


name = 'mnistsmp_09ho';
opt = defopt(name);
opt.seq = {'paramsel:loocvprimal','paramsel:calibratesmp','rls:smp','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,0,0];
opt.process{2} = [3,3,3,2,2];

opt.epochs = 100;
opt.smp_subsize = 6000;
opt.smp_nestimates = 10;
opt.nlambda = 100;

Xtr = norm_zscore(Xtr, [], opt);
Xte = norm_testzscore(Xte, [], opt);

gurls(Xtr, ytr, opt, 1);
gurls(Xte, yte, opt, 2);
