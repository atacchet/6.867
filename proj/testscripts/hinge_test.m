load(fullfile(gurls_root,'demo/data/yeast_data.mat'));

% name = 'hingegd';
% opt = defopt(name);
% opt.seq = {'split:ho','paramsel:hoprimalgd','rls:primalgd','pred:primal','perf:macroavg'};
% opt.process{1} = [2,2,2,0,0];
% opt.process{2} = [3,3,3,2,2];
% gurls (Xtr, ytr, opt,1);
% gurls (Xte, yte, opt,2);

name = 'hingegd_dual_01'
%NOTE : should change 'rls_dualhingegd' to 'hinge_dualgd'
opt = defopt(name);
opt.gd.maxiter = 2000;
% opt.seq = {'split:ho','kernel:linear','paramsel:hodualgd','rls:dualgd','pred:dual','perf:macroavg'};
opt.seq = {'split:ho','kernel:linear','paramsel:hodualhingegd','rls:dualhingegd','pred:dual','perf:macroavg'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];
gurls (Xtr, ytr, opt,1);
gurls (Xte, yte, opt,2);

load hingegd_dual_01
res_hinge = opt;

name = 'hingegd_primal_01'
%NOTE : should change 'rls_dualhingegd' to 'hinge_dualgd'
opt = defopt(name);
opt.gd.maxiter = 2000;
% opt.seq = {'split:ho','kernel:linear','paramsel:hodualgd','rls:dualgd','pred:dual','perf:macroavg'};
opt.seq = {'split:ho','kernel:linear','paramsel:hoprimalhingegd','rls:primalhingegd','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];
gurls (Xtr, ytr, opt,1);
gurls (Xte, yte, opt,2);

load hingegd_primal_01
res_hinge_primal= opt;

% name = 'gd_dual_01'
% 
% %NOTE : should change 'rls_dualhingegd' to 'hinge_dualgd'
% opt = defopt(name);
% opt.seq = {'split:ho','kernel:linear','paramsel:hodualgd','rls:dualgd','pred:dual','perf:macroavg'};
% opt.process{1} = [2,2,2,2,0,0];
% opt.process{2} = [3,3,3,3,2,2];
% gurls (Xtr, ytr, opt,1);
% gurls (Xte, yte, opt,2);
% 
% load gd_dual_01
% res_gd = opt;

% res_gd.perf
res_hinge.perf
res_hinge_primal.perf

% res_gd.paramsel
res_hinge.paramsel
res_hinge_primal.paramsel
