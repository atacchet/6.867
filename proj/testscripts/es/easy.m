load('../big/dsetlesbegues.mat');

ytr = ytr + 0.1 * randn(size(ytr));

name = 'sgdesregression';
opt = defopt(name);
opt.hoperf = @perf_rmse;

opt.seq = {'paramsel:calibratesmp','split:ho','paramsel:sgdes','rls:sgdes','pred:primal','perf:rmse'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];

opt.smp_subsize = 6000;
opt.sgd.subsize = 3e3;
opt.smp_nestimates = 10;
opt.sgdes.numepochs = 10;
opt.hoproportion = 0.01;

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);
