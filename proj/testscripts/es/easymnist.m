load('../smp/mnist_train');
load('../smp/mnist_test');
Xtr = double(Xtr);
Xte = double(Xte);

Xtr = Xtr(ytr == 4 | ytr == 9,:);
Xte = Xte(yte == 4 | yte == 9,:);
ytr = ytr(ytr == 4 | ytr == 9);
yte = yte(yte == 4 | yte == 9);

ytr(ytr == 4) = -1;
ytr(ytr == 9) = 1;

yte(yte == 4) = -1;
yte(yte == 9) = 1;

name = 'mnistes';
opt = defopt(name);
opt.hoperf = @perf_macroavg;

opt.seq = {'paramsel:calibratesmp','split:ho','paramsel:sgdes','rls:sgdes','pred:primal','perf:macroavg'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];

opt.smp_subsize = 6000;
opt.sgd.subsize = 3e3;
opt.smp_nestimates = 10;
opt.sgdes.numepochs = 10;
opt.hoproportion = 0.05;

Xtr = norm_zscore(Xtr, [], opt);
Xte = norm_testzscore(Xte, [], opt);

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);
