load('../big/dsetlesbegues.mat');

idx = randsample(size(Xtr,1),100);
Xtr = Xtr(idx,:);
ytr = ytr(idx,:);


name = 'sgdesregressionsmall';
opt = defopt(name);
opt.hoperf = @perf_rmse;

opt.seq = {'paramsel:calibratesmp','split:ho','paramsel:sgdes','rls:sgdes','pred:primal','perf:rmse'};
opt.process{1} = [2,2,2,2,0,0];
opt.process{2} = [3,3,3,3,2,2];

opt.smp_subsize = 100;
opt.sgd.subsize = 3e3;
opt.smp_nestimates = 10;
opt.sgdes.numepochs = 5000;
opt.hoproportion = 0.7;

gurls(Xtr, ytr, opt,1);
gurls(Xte, yte, opt,2);
