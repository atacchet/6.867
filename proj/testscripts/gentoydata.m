function gentoydata(fun, xtr, xte, opt)

for i = 1:size(xtr,1)
		y = fun(xtr(i,:));
		x = opt.featuremap(xtr(i,:));

		if opt.noise
			y = y + opt.noisesigma*randn(1);
		end

		Xtr(i,:) = x;
		ytr(i,:) = y;
		fprintf('Processing point %d: %.2f%%\r',i, i*100/size(xtr,1));
end
fprintf('\n');
for i = 1:size(xte,1)
		y = fun(xte(i,:));
		x = opt.featuremap(xte(i,:));
		Xte(i,:) = x;
		yte(i,:) = y;
		fprintf('Processing point %d: %.2f%%\r',i, i*100/size(xte,1));
end
	

fprintf('\n');
%ytr = ytr - mean(ytr);
%yte = yte - mean(ytr);

save('big/dsetlesbegues','Xtr','ytr','Xte','yte');
save('big/origlesbegues','xtr','xte');
