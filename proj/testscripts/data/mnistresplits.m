function s = mnistresplits
	load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_train');
	load('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist_test');

	X = [Xtr; Xte];
	y = [ytr; yte];

	c = 2*eye(10) - 1;

	y = c(y,:);

	opt = defopt('dummy');
	opt.nholdouts = 5;
	opt.hoproportion = 0.15;
	s = split_ho([], y, opt);

	for i = 1:opt.nholdouts
		Xtr = X(s{i}.tr,:);
		ytr = y(s{i}.tr,:);
		Xte = X(s{i}.va,:);
		yte = y(s{i}.va,:);

		save(fullfile('/cbcl/cbcl01/atacchet/6.867/proj/testscripts/data/mnist',['mtt' num2str(i)]),...
						'Xtr','Xte','ytr','yte');
	end

