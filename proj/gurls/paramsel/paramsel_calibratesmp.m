function [params] = paramsel_calibratesmp(X, y, opt)

% paramsel_calibratesgd(X,y,opt)
% Computes soem parameters needed by rls_smp (calibrate struct).
%
% NEEDS:
% - opt.smp_nestimates
% - opt.smp_subsize

%% Subsample a set of 6000 examples; We can change this later
%% Do 10 subsamples and ten estimates.


if isfield (opt,'paramsel')
	params = opt.paramsel; % lets not overwrite existing parameters.
			      		 % unless they have the same name
end
[n,d] = size(X);

T = size(y,2);

for i = 1:opt.smp_nestimates,

	idx = randsample(n, opt.smp_subsize);
	M = X(idx,:);
	
	if size(M,1) < size(M,2)
		K = M*M';
	else	
		K = (M'*M);
	end	
	%eigenmax(i) = max(eigs(K));
	rk(i) = rank(K);
	eigenmax(i) = normest(K);
	% eigenmax(i) = trace(K);
end	
params.c = 1/(1-sqrt(mean(rk)/opt.smp_subsize));
params.c = max(params.c,params.c*params.c);
params.eigenmax = params.c*max(eigenmax);
%params.c = 1/(1-(3*sqrt(2)/sqrt(opt.smp_subsize)));
%params.eigenmax = params.c*mean(eigenmax);
params.gamma = 1/(sqrt(3)*params.eigenmax);
params.rk = rk;

