function [params] = paramsel_sgdes(X,y,opt);

if isfield (opt,'paramsel')
	vout = opt.paramsel; % lets not overwrite existing parameters.
			      		 % unless they have the same name
end

if ~iscell(opt.split)
	s = opt.split;
	opt = rmfield(opt, 'split');
	opt.split{1} = s;
end


[n,T]  = size(y);
d = size(X,2);

j = 1;
opt.sgd.tr = opt.split{j}.tr;
opt.sgd.va = opt.split{j}.va;


%sub_size = opt.sgd.subsize;
%idx = randsample(numel(opt.sgd.tr), sub_size);
%M = X(opt.sgd.tr(idx),:);

%smax = normest(M);
%params.eta = 1/(smax);
%params.eta = 1;
params.eta  = opt.paramsel.gamma;
opt.sgd.eta = params.eta;

gen = zeros(T, n*opt.sgdes.numepochs);
opt.sgd.W = zeros(d, T);
params.W_history = zeros(d,T,n*opt.sgdes.numepochs);

for l = 1:opt.sgdes.numepochs*n
		opt.sgd.iter = l;
		p = rls_sgdes_driver(X(opt.sgd.tr,:),y(opt.sgd.tr,:),opt);
		opt.sgd.W = p.W;
		opt.rls.W = p.W;
		% Make predictions
		opt.pred = pred_primal(X(opt.sgd.va,:),[],opt);
		% Get accuracy
		opt.perf = opt.hoperf([],y(opt.sgd.va,:),opt);
		gen(:,l) = opt.perf.forho;
		%%%%%%%%%%%%%%%%%%%%%
		% 		DEBUG		%
		%%%%%%%%%%%%%%%%%%%%%

		params.W_history(:,:,l) = p.W;


		%%%%%%%%%%%%%%%%%%%%%
		%	   END DEBUG	%
		%%%%%%%%%%%%%%%%%%%%%
end
params.W = opt.sgd.W;
params.gen = gen;
