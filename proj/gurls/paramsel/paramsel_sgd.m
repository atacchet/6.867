function params = paramsel_sgd(X,y,opt)
% paramsel_loocvprimal(X,Y,OPT)
% Performs parameter selection when the primal formulation of RLS is used.
% The leave-one-out approach is used.
%
% INPUTS:
% -X: input data matrix
% -Y: labels matrix
% -OPT: structure of options with the following fields with default values
% set through the defopt function:
%		- nlambda
%		- smallnumber
%
%   For more information on standard OPT fields
%   see also defopt
% 
% OUTPUTS: structURE with the following fields:
% -lambdas: array of values of the regularization parameter lambda
%           minimizing the validation error for each class
% -perf: is a matrix with the validation error for each lambda guess 
%        and for each class
% -guesses: array of guesses for the regularization parameter lambda 

if isfield (opt,'paramsel')
	vout = opt.paramsel; % lets not overwrite existing parameters.
			      		 % unless they have the same name
end

[n,T]  = size(y);
d = size(X,2);

sub_size = opt.sgd.subsize;
idx = randsample(n, sub_size);
M = X(idx,:);

smax = normest(M);
L = opt.smallnumber*ones(1,min(n,d));
L(1) = smax;
guesses = n*paramsel_lambdaguesses(L, min(n,d), sub_size, opt);

gen = zeros(n, T, numel(guesses));
params.W = zeros(d, T, numel(guesses));


for l = 1:numel(guesses)
		lambda = guesses(l);
		W = squeeze(params.W(:,:,l));
		W_sum = W;
		params.t0(l) = ceil(norm(X(1,:))/sqrt(lambda));
		t0 = params.t0(l);
		for i = 1:n
			xt = X(i,:);
			yt = y(i,:);

			y_hat = xt*W;
			r = yt - y_hat;
			opt.pred = y_hat;
			tmp = opt.hoperf([],yt,opt);
			gen(i,:,l) = (1/(n - i + 1)) * tmp.forho;

			eta = 1.0/(lambda*(i + t0));
		    W = (1 - lambda*eta)*W + eta*xt'*r;

		    %% Projection onto the ball with radius sqrt(T/lambda)
		    nW = norm(W,'fro');
		    if nW > sqrt(T/lambda)
		        W = (W/nW)*sqrt(T/lambda);
		    end
			W_sum = W_sum + W;
		end
		params.W(:,:,l) = W;
end

params.gen = gen;
params.guesses = guesses;
