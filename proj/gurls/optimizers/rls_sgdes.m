function rls = rls_sgdes(X,y,opt)
		[n,T] = size(y);
		[n,d] = size(X);
		rls.W = zeros(d,T);
		opt.sgd.W = zeros(d,T);
		opt.sgd.eta = opt.paramsel.eta;
		[~,n_iter] = max(opt.paramsel.gen,[],2);
		rls.n_iter = n_iter;

		for l = 1:max(n_iter)
			opt.sgd.iter = l;
			p = rls_sgdes_driver(X,y,opt);
			opt.sgd.W = p.W;
			for t = 1:T
				if n_iter(t) == l
					rls.W(:,t) = opt.sgd.W(:,t);
				end
			end
		end
