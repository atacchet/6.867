function gd = rls_dualhingegd_driver(X,y,opt)

% utility function called by rls_dualgd
% computes a single step for gradient descent algorithm
% NEEDS:
% - opt.gd.method (0 for standard gd, 1 for accelerated)
%	- opt.gd.iter
% - opt.paramsel.eta
%	- opt.kernel.K
%	- opt.kernel.type

gd = opt.gd;
i = opt.gd.iter;
c = opt.gd.c;
if (opt.gd.method == 0)
    arg = y.*(opt.kernel.K*c);
    mask = arg<1;
    c = c + opt.paramsel.eta*opt.kernel.K'*(mask.*y);
else
	error('invalid opt.gd.method')
end
gd.c = c;
