function gd = rls_primalhingegd_driver(X,y,opt)

gd = opt.gd;
i = opt.gd.iter;
W = opt.gd.W;

if (opt.gd.method == 0)
    arg = y.*(X*W);
    mask = arg<1;
	W = W + opt.paramsel.eta*X'*(mask.*y);
else
	error('invalid opt.gd.method')
end
	gd.W = W;
