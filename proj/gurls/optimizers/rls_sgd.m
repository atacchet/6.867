function rls = rls_sgd(X, y, opt)

[n,d] = size(X);
[n,T] = size(y);

g = squeeze(sum(opt.paramsel.gen,1));
[~,idx] = max(g,[],2);
rls.W = zeros(d,T);
for t = 1:T
	rls.W(:,t) = opt.paramsel.W(:,t, idx(t));
end
rls.X = [];
rls.X = [];

