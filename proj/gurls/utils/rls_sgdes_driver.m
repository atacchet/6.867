function rls = rls_sgdes_driver(X,y,opt)

		i = randi(size(y,1),1);
		xt = X(i,:);
		yt = y(i,:);

		y_hat = xt*opt.sgd.W;
		r = yt - y_hat;

%		eta = opt.sgd.eta/opt.sgd.iter;
		eta = opt.sgd.eta;
		rls.W = opt.sgd.W + eta*xt'*r;

