function [cfr] = rls_smp_singlepass(X,y,opt)

cfr = opt.cfr;
w = cfr.W;
r = cfr.R;

wsum = cfr.W_sum;
count = cfr.count;

gamma = opt.paramsel.gamma;


[n,d] = size(X);
T = size(y,2);
seq = randperm(n);


lambda = n*opt.singlelambda(opt.paramsel.lambdas);
for i = 1:n
	count = count + 1;
	x = X(seq(i),:);
	yy = y(seq(i),:);
	w = r + gamma * (x') * (yy - x * r);
	nW = norm(w,'fro');
	if nW > sqrt(T/lambda)
		w = (w/nW)*sqrt(T/lambda);
	end
	
	r = r + gamma * (x') * (yy - x * w);
	nR = norm(r, 'fro');
	if nR > sqrt(T/lambda)
		r = (r/nR) * sqrt(T/lambda);
	end
	wsum = wsum + w;
end

cfr.W = w;
cfr.W_sum = wsum;
cfr.R = r;
cfr.count = count;
