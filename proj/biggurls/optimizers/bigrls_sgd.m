function [rls] = bigrls_sgd(X,y,opt)
		
		d = size(opt.paramsel.W,1);
		T = size(opt.paramsel.W,2);
		%[~, idx] = max(opt.paramsel.gen,[],1);
		[~,idx] = min(sum(opt.paramsel.gen,1));
		rls.W = zeros(d,T);
		rls.W = squeeze(opt.paramsel.W(:,:,idx));
%		for i = 1:T
%			rls.W(:,i) = squeeze(opt.paramsel.W(:,i, idx(i)));
%		end
		rls.C = [];
		rls.X = [];
