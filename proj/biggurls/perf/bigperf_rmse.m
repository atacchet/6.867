function [p] = bigperf_macroavg(X,y,opt)
% bigperf_macroavg(X,y,opt);
% Computes the average classification accuracy per class.
%
% INPUTS:
% -X: input data bigarray
% -y: labels bigarray
% -OPT: structure of options with the following fields (and subfields):
%   fields that need to be set through previous gurls tasks:
%       -pred (set by the pred_* routines)
% 
% OUTPUT: struct with the following fields:
% -acc: array of prediction accuracy for each class
% -forho: ""
% -forplot: ""


		if isfield(opt, 'perf')
			p = opt.perf;
		end

		transpose = opt.pred.Transpose(true);
		T = y.Sizes();
		T = T{1};

		n_class = zeros(1,T);
		mse = zeros(1,T);

		for i1 = 1:y.BlockSize : y.NumItems
			i2 = min(i1 + y.BlockSize -1 , y.NumItems);
			y_block = y(i1 : i2, : );
			ypred_block = opt.pred(i1 : i2, : );

			diff_block = y_block - ypred_block;
			mse = mse + ((1/(i2-i1)) * sum(diff_block.^2,1));

        end
		p.rmse = sqrt(1/y.NumItems);
		

        p.forho = -p.rmse;
        p.forplot = -p.rmse;
		opt.pred.Transpose(transpose);
