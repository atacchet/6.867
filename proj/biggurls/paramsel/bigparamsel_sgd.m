function [params] = bigparamsel_calibratesgd(X, y, opt)

%	Performs parameter selection when one wants to solve the problem using rls_pegasos.
%
% -OPT: structure of options with the following fields with default values  set through the defopt function:
%		- subsize
%		- calibfile
%		- hoperf
%		- singlelambda
%
%   For more information on standard OPT fields
%   see also defopt
% 
% OUTPUTS: structure with the following fields:
% - lambda: selected value for the regularization parameter
% - W: rls coefficient vector

n = X.NumItems();
d = X.Sizes();
d = d{1};

T = y.Sizes();
T = T{1};

opt.cfr.W = zeros(d,T);
opt.cfr.W_sum = zeros(d,T);
opt.cfr.count = 0;

% Generate guesses for lambda
sub_size = opt.sgd.subsize;
idx = randsample(n, sub_size);
M = X(idx,:);
smax = normest(M);
L = opt.smallnumber*ones(1,min(n,d));
L(1) = smax;
guesses = paramsel_lambdaguesses(L, min(n,d), sub_size, opt);

% solve RLS with sgd for every lambda

gen = zeros(numel(guesses), T);
params.W = zeros(d,T,numel(guesses));

for l = 1:numel(guesses)
		lambda = guesses(l);
		%params.t0 = ceil(norm(M(1,:))/sqrt(lambda));
		params.t0 = 0;
		for i = 1:n
			W = squeeze(params.W(:,:,l));
			xt = X(i,:);
			yt = y(i,:);

			y_hat = (xt*W);

			% Only works for regression
	   		r = y_hat - yt;
			opt.pred = y_hat;
			gen(i,l) = (1/(n-i+1))*norm(r);
			%tmp = opt.hoperf([],yt,opt);
			%gen(l,:) = gen(l,:) + tmp.forho;

			eta = 1.0/(params.t0 + lambda*(i));
	    	W = (1 - lambda*eta)*W + eta*xt'*r;

			nW = norm(W,'fro');
	    	if nW > sqrt(T/lambda)
	    	    W = (W/nW)*sqrt(T/lambda);
	    	end
			params.W(:,:,l) = W;
		end
end

params.gen = gen;
params.guesses = guesses;
